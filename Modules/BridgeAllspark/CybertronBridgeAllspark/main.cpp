#include "TheApp.hpp"
#include "cybertron/core/Log.hpp"

//CYBERTRON_BEGIN

int main(int argc, char* argv[])
{
	TheApp p;


	printf("---------------Cybertron Bridge Allspark Start-------------");
	cybertron::setupLogFile("CybertronBridgeAllspark");

	p.init(argc, argv);
	p.create();

	return 0;
}

//CYBERTRON_END
