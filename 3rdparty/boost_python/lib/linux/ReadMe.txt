How to build:
build as static library, for python3.6, use clang, config setting aligned with user project(e.g. like pyHDMap).
1. Download boost_1_67_0.tar.gz. Extract it.
2. >>cd boost_1_67_0
   >>./bootstrap.sh --with-libraries=python --with-python-version=3.6 --with-toolset=clang  
   >>./b2 --with-python toolset=clang cxxflags="-std=gnu++14 -fPIC -stdlib=libc++ -Wno-inconsistent-missing-override -I/usr/include/libcxxabi " linkflags="-stdlib=libc++ -lc++abi " link=static stage address-model=64 threading=multi debug
*Note*: here the cxxflags, linkflags have to be align with pyHDMap project setting, otherwise would either bring link error or execution error, like ImportError undefined symbol: pyHDMap.so: _ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEE12_M_constructEmc.

Change setting to release:
   >>./b2 --with-python toolset=clang cxxflags="-std=gnu++14 -fPIC -stdlib=libc++ -Wno-inconsistent-missing-override -I/usr/include/libcxxabi " linkflags="-stdlib=libc++ -lc++abi " link=static stage address-model=64 threading=multi release