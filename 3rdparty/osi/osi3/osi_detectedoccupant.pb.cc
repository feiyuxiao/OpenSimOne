// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: osi_detectedoccupant.proto

#include "osi_detectedoccupant.pb.h"

#include <algorithm>

#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/extension_set.h>
#include <google/protobuf/wire_format_lite.h>
#include <google/protobuf/descriptor.h>
#include <google/protobuf/generated_message_reflection.h>
#include <google/protobuf/reflection_ops.h>
#include <google/protobuf/wire_format.h>
// @@protoc_insertion_point(includes)
#include <google/protobuf/port_def.inc>
extern PROTOBUF_INTERNAL_EXPORT_osi_5fdetectedobject_2eproto ::PROTOBUF_NAMESPACE_ID::internal::SCCInfo<1> scc_info_DetectedItemHeader_osi_5fdetectedobject_2eproto;
extern PROTOBUF_INTERNAL_EXPORT_osi_5fdetectedoccupant_2eproto ::PROTOBUF_NAMESPACE_ID::internal::SCCInfo<1> scc_info_DetectedOccupant_CandidateOccupant_osi_5fdetectedoccupant_2eproto;
extern PROTOBUF_INTERNAL_EXPORT_osi_5foccupant_2eproto ::PROTOBUF_NAMESPACE_ID::internal::SCCInfo<0> scc_info_Occupant_Classification_osi_5foccupant_2eproto;
namespace osi3 {
class DetectedOccupant_CandidateOccupantDefaultTypeInternal {
 public:
  ::PROTOBUF_NAMESPACE_ID::internal::ExplicitlyConstructed<DetectedOccupant_CandidateOccupant> _instance;
} _DetectedOccupant_CandidateOccupant_default_instance_;
class DetectedOccupantDefaultTypeInternal {
 public:
  ::PROTOBUF_NAMESPACE_ID::internal::ExplicitlyConstructed<DetectedOccupant> _instance;
} _DetectedOccupant_default_instance_;
}  // namespace osi3
static void InitDefaultsscc_info_DetectedOccupant_osi_5fdetectedoccupant_2eproto() {
  GOOGLE_PROTOBUF_VERIFY_VERSION;

  {
    void* ptr = &::osi3::_DetectedOccupant_default_instance_;
    new (ptr) ::osi3::DetectedOccupant();
    ::PROTOBUF_NAMESPACE_ID::internal::OnShutdownDestroyMessage(ptr);
  }
  ::osi3::DetectedOccupant::InitAsDefaultInstance();
}

::PROTOBUF_NAMESPACE_ID::internal::SCCInfo<2> scc_info_DetectedOccupant_osi_5fdetectedoccupant_2eproto =
    {{ATOMIC_VAR_INIT(::PROTOBUF_NAMESPACE_ID::internal::SCCInfoBase::kUninitialized), 2, 0, InitDefaultsscc_info_DetectedOccupant_osi_5fdetectedoccupant_2eproto}, {
      &scc_info_DetectedItemHeader_osi_5fdetectedobject_2eproto.base,
      &scc_info_DetectedOccupant_CandidateOccupant_osi_5fdetectedoccupant_2eproto.base,}};

static void InitDefaultsscc_info_DetectedOccupant_CandidateOccupant_osi_5fdetectedoccupant_2eproto() {
  GOOGLE_PROTOBUF_VERIFY_VERSION;

  {
    void* ptr = &::osi3::_DetectedOccupant_CandidateOccupant_default_instance_;
    new (ptr) ::osi3::DetectedOccupant_CandidateOccupant();
    ::PROTOBUF_NAMESPACE_ID::internal::OnShutdownDestroyMessage(ptr);
  }
  ::osi3::DetectedOccupant_CandidateOccupant::InitAsDefaultInstance();
}

::PROTOBUF_NAMESPACE_ID::internal::SCCInfo<1> scc_info_DetectedOccupant_CandidateOccupant_osi_5fdetectedoccupant_2eproto =
    {{ATOMIC_VAR_INIT(::PROTOBUF_NAMESPACE_ID::internal::SCCInfoBase::kUninitialized), 1, 0, InitDefaultsscc_info_DetectedOccupant_CandidateOccupant_osi_5fdetectedoccupant_2eproto}, {
      &scc_info_Occupant_Classification_osi_5foccupant_2eproto.base,}};

static ::PROTOBUF_NAMESPACE_ID::Metadata file_level_metadata_osi_5fdetectedoccupant_2eproto[2];
static constexpr ::PROTOBUF_NAMESPACE_ID::EnumDescriptor const** file_level_enum_descriptors_osi_5fdetectedoccupant_2eproto = nullptr;
static constexpr ::PROTOBUF_NAMESPACE_ID::ServiceDescriptor const** file_level_service_descriptors_osi_5fdetectedoccupant_2eproto = nullptr;

const ::PROTOBUF_NAMESPACE_ID::uint32 TableStruct_osi_5fdetectedoccupant_2eproto::offsets[] PROTOBUF_SECTION_VARIABLE(protodesc_cold) = {
  ~0u,  // no _has_bits_
  PROTOBUF_FIELD_OFFSET(::osi3::DetectedOccupant_CandidateOccupant, _internal_metadata_),
  ~0u,  // no _extensions_
  ~0u,  // no _oneof_case_
  ~0u,  // no _weak_field_map_
  PROTOBUF_FIELD_OFFSET(::osi3::DetectedOccupant_CandidateOccupant, probability_),
  PROTOBUF_FIELD_OFFSET(::osi3::DetectedOccupant_CandidateOccupant, classification_),
  ~0u,  // no _has_bits_
  PROTOBUF_FIELD_OFFSET(::osi3::DetectedOccupant, _internal_metadata_),
  ~0u,  // no _extensions_
  ~0u,  // no _oneof_case_
  ~0u,  // no _weak_field_map_
  PROTOBUF_FIELD_OFFSET(::osi3::DetectedOccupant, header_),
  PROTOBUF_FIELD_OFFSET(::osi3::DetectedOccupant, candidate_),
};
static const ::PROTOBUF_NAMESPACE_ID::internal::MigrationSchema schemas[] PROTOBUF_SECTION_VARIABLE(protodesc_cold) = {
  { 0, -1, sizeof(::osi3::DetectedOccupant_CandidateOccupant)},
  { 7, -1, sizeof(::osi3::DetectedOccupant)},
};

static ::PROTOBUF_NAMESPACE_ID::Message const * const file_default_instances[] = {
  reinterpret_cast<const ::PROTOBUF_NAMESPACE_ID::Message*>(&::osi3::_DetectedOccupant_CandidateOccupant_default_instance_),
  reinterpret_cast<const ::PROTOBUF_NAMESPACE_ID::Message*>(&::osi3::_DetectedOccupant_default_instance_),
};

const char descriptor_table_protodef_osi_5fdetectedoccupant_2eproto[] PROTOBUF_SECTION_VARIABLE(protodesc_cold) =
  "\n\032osi_detectedoccupant.proto\022\004osi3\032\022osi_"
  "occupant.proto\032\030osi_detectedobject.proto"
  "\"\332\001\n\020DetectedOccupant\022(\n\006header\030\001 \001(\0132\030."
  "osi3.DetectedItemHeader\022;\n\tcandidate\030\002 \003"
  "(\0132(.osi3.DetectedOccupant.CandidateOccu"
  "pant\032_\n\021CandidateOccupant\022\023\n\013probability"
  "\030\001 \001(\001\0225\n\016classification\030\002 \001(\0132\035.osi3.Oc"
  "cupant.ClassificationB\002H\001b\006proto3"
  ;
static const ::PROTOBUF_NAMESPACE_ID::internal::DescriptorTable*const descriptor_table_osi_5fdetectedoccupant_2eproto_deps[2] = {
  &::descriptor_table_osi_5fdetectedobject_2eproto,
  &::descriptor_table_osi_5foccupant_2eproto,
};
static ::PROTOBUF_NAMESPACE_ID::internal::SCCInfoBase*const descriptor_table_osi_5fdetectedoccupant_2eproto_sccs[2] = {
  &scc_info_DetectedOccupant_osi_5fdetectedoccupant_2eproto.base,
  &scc_info_DetectedOccupant_CandidateOccupant_osi_5fdetectedoccupant_2eproto.base,
};
static ::PROTOBUF_NAMESPACE_ID::internal::once_flag descriptor_table_osi_5fdetectedoccupant_2eproto_once;
const ::PROTOBUF_NAMESPACE_ID::internal::DescriptorTable descriptor_table_osi_5fdetectedoccupant_2eproto = {
  false, false, descriptor_table_protodef_osi_5fdetectedoccupant_2eproto, "osi_detectedoccupant.proto", 313,
  &descriptor_table_osi_5fdetectedoccupant_2eproto_once, descriptor_table_osi_5fdetectedoccupant_2eproto_sccs, descriptor_table_osi_5fdetectedoccupant_2eproto_deps, 2, 2,
  schemas, file_default_instances, TableStruct_osi_5fdetectedoccupant_2eproto::offsets,
  file_level_metadata_osi_5fdetectedoccupant_2eproto, 2, file_level_enum_descriptors_osi_5fdetectedoccupant_2eproto, file_level_service_descriptors_osi_5fdetectedoccupant_2eproto,
};

// Force running AddDescriptors() at dynamic initialization time.
static bool dynamic_init_dummy_osi_5fdetectedoccupant_2eproto = (static_cast<void>(::PROTOBUF_NAMESPACE_ID::internal::AddDescriptors(&descriptor_table_osi_5fdetectedoccupant_2eproto)), true);
namespace osi3 {

// ===================================================================

void DetectedOccupant_CandidateOccupant::InitAsDefaultInstance() {
  ::osi3::_DetectedOccupant_CandidateOccupant_default_instance_._instance.get_mutable()->classification_ = const_cast< ::osi3::Occupant_Classification*>(
      ::osi3::Occupant_Classification::internal_default_instance());
}
class DetectedOccupant_CandidateOccupant::_Internal {
 public:
  static const ::osi3::Occupant_Classification& classification(const DetectedOccupant_CandidateOccupant* msg);
};

const ::osi3::Occupant_Classification&
DetectedOccupant_CandidateOccupant::_Internal::classification(const DetectedOccupant_CandidateOccupant* msg) {
  return *msg->classification_;
}
void DetectedOccupant_CandidateOccupant::clear_classification() {
  if (GetArena() == nullptr && classification_ != nullptr) {
    delete classification_;
  }
  classification_ = nullptr;
}
DetectedOccupant_CandidateOccupant::DetectedOccupant_CandidateOccupant(::PROTOBUF_NAMESPACE_ID::Arena* arena)
  : ::PROTOBUF_NAMESPACE_ID::Message(arena) {
  SharedCtor();
  RegisterArenaDtor(arena);
  // @@protoc_insertion_point(arena_constructor:osi3.DetectedOccupant.CandidateOccupant)
}
DetectedOccupant_CandidateOccupant::DetectedOccupant_CandidateOccupant(const DetectedOccupant_CandidateOccupant& from)
  : ::PROTOBUF_NAMESPACE_ID::Message() {
  _internal_metadata_.MergeFrom<::PROTOBUF_NAMESPACE_ID::UnknownFieldSet>(from._internal_metadata_);
  if (from._internal_has_classification()) {
    classification_ = new ::osi3::Occupant_Classification(*from.classification_);
  } else {
    classification_ = nullptr;
  }
  probability_ = from.probability_;
  // @@protoc_insertion_point(copy_constructor:osi3.DetectedOccupant.CandidateOccupant)
}

void DetectedOccupant_CandidateOccupant::SharedCtor() {
  ::PROTOBUF_NAMESPACE_ID::internal::InitSCC(&scc_info_DetectedOccupant_CandidateOccupant_osi_5fdetectedoccupant_2eproto.base);
  ::memset(&classification_, 0, static_cast<size_t>(
      reinterpret_cast<char*>(&probability_) -
      reinterpret_cast<char*>(&classification_)) + sizeof(probability_));
}

DetectedOccupant_CandidateOccupant::~DetectedOccupant_CandidateOccupant() {
  // @@protoc_insertion_point(destructor:osi3.DetectedOccupant.CandidateOccupant)
  SharedDtor();
  _internal_metadata_.Delete<::PROTOBUF_NAMESPACE_ID::UnknownFieldSet>();
}

void DetectedOccupant_CandidateOccupant::SharedDtor() {
  GOOGLE_DCHECK(GetArena() == nullptr);
  if (this != internal_default_instance()) delete classification_;
}

void DetectedOccupant_CandidateOccupant::ArenaDtor(void* object) {
  DetectedOccupant_CandidateOccupant* _this = reinterpret_cast< DetectedOccupant_CandidateOccupant* >(object);
  (void)_this;
}
void DetectedOccupant_CandidateOccupant::RegisterArenaDtor(::PROTOBUF_NAMESPACE_ID::Arena*) {
}
void DetectedOccupant_CandidateOccupant::SetCachedSize(int size) const {
  _cached_size_.Set(size);
}
const DetectedOccupant_CandidateOccupant& DetectedOccupant_CandidateOccupant::default_instance() {
  ::PROTOBUF_NAMESPACE_ID::internal::InitSCC(&::scc_info_DetectedOccupant_CandidateOccupant_osi_5fdetectedoccupant_2eproto.base);
  return *internal_default_instance();
}


void DetectedOccupant_CandidateOccupant::Clear() {
// @@protoc_insertion_point(message_clear_start:osi3.DetectedOccupant.CandidateOccupant)
  ::PROTOBUF_NAMESPACE_ID::uint32 cached_has_bits = 0;
  // Prevent compiler warnings about cached_has_bits being unused
  (void) cached_has_bits;

  if (GetArena() == nullptr && classification_ != nullptr) {
    delete classification_;
  }
  classification_ = nullptr;
  probability_ = 0;
  _internal_metadata_.Clear<::PROTOBUF_NAMESPACE_ID::UnknownFieldSet>();
}

const char* DetectedOccupant_CandidateOccupant::_InternalParse(const char* ptr, ::PROTOBUF_NAMESPACE_ID::internal::ParseContext* ctx) {
#define CHK_(x) if (PROTOBUF_PREDICT_FALSE(!(x))) goto failure
  ::PROTOBUF_NAMESPACE_ID::Arena* arena = GetArena(); (void)arena;
  while (!ctx->Done(&ptr)) {
    ::PROTOBUF_NAMESPACE_ID::uint32 tag;
    ptr = ::PROTOBUF_NAMESPACE_ID::internal::ReadTag(ptr, &tag);
    CHK_(ptr);
    switch (tag >> 3) {
      // double probability = 1;
      case 1:
        if (PROTOBUF_PREDICT_TRUE(static_cast<::PROTOBUF_NAMESPACE_ID::uint8>(tag) == 9)) {
          probability_ = ::PROTOBUF_NAMESPACE_ID::internal::UnalignedLoad<double>(ptr);
          ptr += sizeof(double);
        } else goto handle_unusual;
        continue;
      // .osi3.Occupant.Classification classification = 2;
      case 2:
        if (PROTOBUF_PREDICT_TRUE(static_cast<::PROTOBUF_NAMESPACE_ID::uint8>(tag) == 18)) {
          ptr = ctx->ParseMessage(_internal_mutable_classification(), ptr);
          CHK_(ptr);
        } else goto handle_unusual;
        continue;
      default: {
      handle_unusual:
        if ((tag & 7) == 4 || tag == 0) {
          ctx->SetLastTag(tag);
          goto success;
        }
        ptr = UnknownFieldParse(tag,
            _internal_metadata_.mutable_unknown_fields<::PROTOBUF_NAMESPACE_ID::UnknownFieldSet>(),
            ptr, ctx);
        CHK_(ptr != nullptr);
        continue;
      }
    }  // switch
  }  // while
success:
  return ptr;
failure:
  ptr = nullptr;
  goto success;
#undef CHK_
}

::PROTOBUF_NAMESPACE_ID::uint8* DetectedOccupant_CandidateOccupant::_InternalSerialize(
    ::PROTOBUF_NAMESPACE_ID::uint8* target, ::PROTOBUF_NAMESPACE_ID::io::EpsCopyOutputStream* stream) const {
  // @@protoc_insertion_point(serialize_to_array_start:osi3.DetectedOccupant.CandidateOccupant)
  ::PROTOBUF_NAMESPACE_ID::uint32 cached_has_bits = 0;
  (void) cached_has_bits;

  // double probability = 1;
  if (!(this->probability() <= 0 && this->probability() >= 0)) {
    target = stream->EnsureSpace(target);
    target = ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::WriteDoubleToArray(1, this->_internal_probability(), target);
  }

  // .osi3.Occupant.Classification classification = 2;
  if (this->has_classification()) {
    target = stream->EnsureSpace(target);
    target = ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::
      InternalWriteMessage(
        2, _Internal::classification(this), target, stream);
  }

  if (PROTOBUF_PREDICT_FALSE(_internal_metadata_.have_unknown_fields())) {
    target = ::PROTOBUF_NAMESPACE_ID::internal::WireFormat::InternalSerializeUnknownFieldsToArray(
        _internal_metadata_.unknown_fields<::PROTOBUF_NAMESPACE_ID::UnknownFieldSet>(::PROTOBUF_NAMESPACE_ID::UnknownFieldSet::default_instance), target, stream);
  }
  // @@protoc_insertion_point(serialize_to_array_end:osi3.DetectedOccupant.CandidateOccupant)
  return target;
}

size_t DetectedOccupant_CandidateOccupant::ByteSizeLong() const {
// @@protoc_insertion_point(message_byte_size_start:osi3.DetectedOccupant.CandidateOccupant)
  size_t total_size = 0;

  ::PROTOBUF_NAMESPACE_ID::uint32 cached_has_bits = 0;
  // Prevent compiler warnings about cached_has_bits being unused
  (void) cached_has_bits;

  // .osi3.Occupant.Classification classification = 2;
  if (this->has_classification()) {
    total_size += 1 +
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::MessageSize(
        *classification_);
  }

  // double probability = 1;
  if (!(this->probability() <= 0 && this->probability() >= 0)) {
    total_size += 1 + 8;
  }

  if (PROTOBUF_PREDICT_FALSE(_internal_metadata_.have_unknown_fields())) {
    return ::PROTOBUF_NAMESPACE_ID::internal::ComputeUnknownFieldsSize(
        _internal_metadata_, total_size, &_cached_size_);
  }
  int cached_size = ::PROTOBUF_NAMESPACE_ID::internal::ToCachedSize(total_size);
  SetCachedSize(cached_size);
  return total_size;
}

void DetectedOccupant_CandidateOccupant::MergeFrom(const ::PROTOBUF_NAMESPACE_ID::Message& from) {
// @@protoc_insertion_point(generalized_merge_from_start:osi3.DetectedOccupant.CandidateOccupant)
  GOOGLE_DCHECK_NE(&from, this);
  const DetectedOccupant_CandidateOccupant* source =
      ::PROTOBUF_NAMESPACE_ID::DynamicCastToGenerated<DetectedOccupant_CandidateOccupant>(
          &from);
  if (source == nullptr) {
  // @@protoc_insertion_point(generalized_merge_from_cast_fail:osi3.DetectedOccupant.CandidateOccupant)
    ::PROTOBUF_NAMESPACE_ID::internal::ReflectionOps::Merge(from, this);
  } else {
  // @@protoc_insertion_point(generalized_merge_from_cast_success:osi3.DetectedOccupant.CandidateOccupant)
    MergeFrom(*source);
  }
}

void DetectedOccupant_CandidateOccupant::MergeFrom(const DetectedOccupant_CandidateOccupant& from) {
// @@protoc_insertion_point(class_specific_merge_from_start:osi3.DetectedOccupant.CandidateOccupant)
  GOOGLE_DCHECK_NE(&from, this);
  _internal_metadata_.MergeFrom<::PROTOBUF_NAMESPACE_ID::UnknownFieldSet>(from._internal_metadata_);
  ::PROTOBUF_NAMESPACE_ID::uint32 cached_has_bits = 0;
  (void) cached_has_bits;

  if (from.has_classification()) {
    _internal_mutable_classification()->::osi3::Occupant_Classification::MergeFrom(from._internal_classification());
  }
  if (!(from.probability() <= 0 && from.probability() >= 0)) {
    _internal_set_probability(from._internal_probability());
  }
}

void DetectedOccupant_CandidateOccupant::CopyFrom(const ::PROTOBUF_NAMESPACE_ID::Message& from) {
// @@protoc_insertion_point(generalized_copy_from_start:osi3.DetectedOccupant.CandidateOccupant)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

void DetectedOccupant_CandidateOccupant::CopyFrom(const DetectedOccupant_CandidateOccupant& from) {
// @@protoc_insertion_point(class_specific_copy_from_start:osi3.DetectedOccupant.CandidateOccupant)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

bool DetectedOccupant_CandidateOccupant::IsInitialized() const {
  return true;
}

void DetectedOccupant_CandidateOccupant::InternalSwap(DetectedOccupant_CandidateOccupant* other) {
  using std::swap;
  _internal_metadata_.Swap<::PROTOBUF_NAMESPACE_ID::UnknownFieldSet>(&other->_internal_metadata_);
  ::PROTOBUF_NAMESPACE_ID::internal::memswap<
      PROTOBUF_FIELD_OFFSET(DetectedOccupant_CandidateOccupant, probability_)
      + sizeof(DetectedOccupant_CandidateOccupant::probability_)
      - PROTOBUF_FIELD_OFFSET(DetectedOccupant_CandidateOccupant, classification_)>(
          reinterpret_cast<char*>(&classification_),
          reinterpret_cast<char*>(&other->classification_));
}

::PROTOBUF_NAMESPACE_ID::Metadata DetectedOccupant_CandidateOccupant::GetMetadata() const {
  return GetMetadataStatic();
}


// ===================================================================

void DetectedOccupant::InitAsDefaultInstance() {
  ::osi3::_DetectedOccupant_default_instance_._instance.get_mutable()->header_ = const_cast< ::osi3::DetectedItemHeader*>(
      ::osi3::DetectedItemHeader::internal_default_instance());
}
class DetectedOccupant::_Internal {
 public:
  static const ::osi3::DetectedItemHeader& header(const DetectedOccupant* msg);
};

const ::osi3::DetectedItemHeader&
DetectedOccupant::_Internal::header(const DetectedOccupant* msg) {
  return *msg->header_;
}
void DetectedOccupant::clear_header() {
  if (GetArena() == nullptr && header_ != nullptr) {
    delete header_;
  }
  header_ = nullptr;
}
DetectedOccupant::DetectedOccupant(::PROTOBUF_NAMESPACE_ID::Arena* arena)
  : ::PROTOBUF_NAMESPACE_ID::Message(arena),
  candidate_(arena) {
  SharedCtor();
  RegisterArenaDtor(arena);
  // @@protoc_insertion_point(arena_constructor:osi3.DetectedOccupant)
}
DetectedOccupant::DetectedOccupant(const DetectedOccupant& from)
  : ::PROTOBUF_NAMESPACE_ID::Message(),
      candidate_(from.candidate_) {
  _internal_metadata_.MergeFrom<::PROTOBUF_NAMESPACE_ID::UnknownFieldSet>(from._internal_metadata_);
  if (from._internal_has_header()) {
    header_ = new ::osi3::DetectedItemHeader(*from.header_);
  } else {
    header_ = nullptr;
  }
  // @@protoc_insertion_point(copy_constructor:osi3.DetectedOccupant)
}

void DetectedOccupant::SharedCtor() {
  ::PROTOBUF_NAMESPACE_ID::internal::InitSCC(&scc_info_DetectedOccupant_osi_5fdetectedoccupant_2eproto.base);
  header_ = nullptr;
}

DetectedOccupant::~DetectedOccupant() {
  // @@protoc_insertion_point(destructor:osi3.DetectedOccupant)
  SharedDtor();
  _internal_metadata_.Delete<::PROTOBUF_NAMESPACE_ID::UnknownFieldSet>();
}

void DetectedOccupant::SharedDtor() {
  GOOGLE_DCHECK(GetArena() == nullptr);
  if (this != internal_default_instance()) delete header_;
}

void DetectedOccupant::ArenaDtor(void* object) {
  DetectedOccupant* _this = reinterpret_cast< DetectedOccupant* >(object);
  (void)_this;
}
void DetectedOccupant::RegisterArenaDtor(::PROTOBUF_NAMESPACE_ID::Arena*) {
}
void DetectedOccupant::SetCachedSize(int size) const {
  _cached_size_.Set(size);
}
const DetectedOccupant& DetectedOccupant::default_instance() {
  ::PROTOBUF_NAMESPACE_ID::internal::InitSCC(&::scc_info_DetectedOccupant_osi_5fdetectedoccupant_2eproto.base);
  return *internal_default_instance();
}


void DetectedOccupant::Clear() {
// @@protoc_insertion_point(message_clear_start:osi3.DetectedOccupant)
  ::PROTOBUF_NAMESPACE_ID::uint32 cached_has_bits = 0;
  // Prevent compiler warnings about cached_has_bits being unused
  (void) cached_has_bits;

  candidate_.Clear();
  if (GetArena() == nullptr && header_ != nullptr) {
    delete header_;
  }
  header_ = nullptr;
  _internal_metadata_.Clear<::PROTOBUF_NAMESPACE_ID::UnknownFieldSet>();
}

const char* DetectedOccupant::_InternalParse(const char* ptr, ::PROTOBUF_NAMESPACE_ID::internal::ParseContext* ctx) {
#define CHK_(x) if (PROTOBUF_PREDICT_FALSE(!(x))) goto failure
  ::PROTOBUF_NAMESPACE_ID::Arena* arena = GetArena(); (void)arena;
  while (!ctx->Done(&ptr)) {
    ::PROTOBUF_NAMESPACE_ID::uint32 tag;
    ptr = ::PROTOBUF_NAMESPACE_ID::internal::ReadTag(ptr, &tag);
    CHK_(ptr);
    switch (tag >> 3) {
      // .osi3.DetectedItemHeader header = 1;
      case 1:
        if (PROTOBUF_PREDICT_TRUE(static_cast<::PROTOBUF_NAMESPACE_ID::uint8>(tag) == 10)) {
          ptr = ctx->ParseMessage(_internal_mutable_header(), ptr);
          CHK_(ptr);
        } else goto handle_unusual;
        continue;
      // repeated .osi3.DetectedOccupant.CandidateOccupant candidate = 2;
      case 2:
        if (PROTOBUF_PREDICT_TRUE(static_cast<::PROTOBUF_NAMESPACE_ID::uint8>(tag) == 18)) {
          ptr -= 1;
          do {
            ptr += 1;
            ptr = ctx->ParseMessage(_internal_add_candidate(), ptr);
            CHK_(ptr);
            if (!ctx->DataAvailable(ptr)) break;
          } while (::PROTOBUF_NAMESPACE_ID::internal::ExpectTag<18>(ptr));
        } else goto handle_unusual;
        continue;
      default: {
      handle_unusual:
        if ((tag & 7) == 4 || tag == 0) {
          ctx->SetLastTag(tag);
          goto success;
        }
        ptr = UnknownFieldParse(tag,
            _internal_metadata_.mutable_unknown_fields<::PROTOBUF_NAMESPACE_ID::UnknownFieldSet>(),
            ptr, ctx);
        CHK_(ptr != nullptr);
        continue;
      }
    }  // switch
  }  // while
success:
  return ptr;
failure:
  ptr = nullptr;
  goto success;
#undef CHK_
}

::PROTOBUF_NAMESPACE_ID::uint8* DetectedOccupant::_InternalSerialize(
    ::PROTOBUF_NAMESPACE_ID::uint8* target, ::PROTOBUF_NAMESPACE_ID::io::EpsCopyOutputStream* stream) const {
  // @@protoc_insertion_point(serialize_to_array_start:osi3.DetectedOccupant)
  ::PROTOBUF_NAMESPACE_ID::uint32 cached_has_bits = 0;
  (void) cached_has_bits;

  // .osi3.DetectedItemHeader header = 1;
  if (this->has_header()) {
    target = stream->EnsureSpace(target);
    target = ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::
      InternalWriteMessage(
        1, _Internal::header(this), target, stream);
  }

  // repeated .osi3.DetectedOccupant.CandidateOccupant candidate = 2;
  for (unsigned int i = 0,
      n = static_cast<unsigned int>(this->_internal_candidate_size()); i < n; i++) {
    target = stream->EnsureSpace(target);
    target = ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::
      InternalWriteMessage(2, this->_internal_candidate(i), target, stream);
  }

  if (PROTOBUF_PREDICT_FALSE(_internal_metadata_.have_unknown_fields())) {
    target = ::PROTOBUF_NAMESPACE_ID::internal::WireFormat::InternalSerializeUnknownFieldsToArray(
        _internal_metadata_.unknown_fields<::PROTOBUF_NAMESPACE_ID::UnknownFieldSet>(::PROTOBUF_NAMESPACE_ID::UnknownFieldSet::default_instance), target, stream);
  }
  // @@protoc_insertion_point(serialize_to_array_end:osi3.DetectedOccupant)
  return target;
}

size_t DetectedOccupant::ByteSizeLong() const {
// @@protoc_insertion_point(message_byte_size_start:osi3.DetectedOccupant)
  size_t total_size = 0;

  ::PROTOBUF_NAMESPACE_ID::uint32 cached_has_bits = 0;
  // Prevent compiler warnings about cached_has_bits being unused
  (void) cached_has_bits;

  // repeated .osi3.DetectedOccupant.CandidateOccupant candidate = 2;
  total_size += 1UL * this->_internal_candidate_size();
  for (const auto& msg : this->candidate_) {
    total_size +=
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::MessageSize(msg);
  }

  // .osi3.DetectedItemHeader header = 1;
  if (this->has_header()) {
    total_size += 1 +
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::MessageSize(
        *header_);
  }

  if (PROTOBUF_PREDICT_FALSE(_internal_metadata_.have_unknown_fields())) {
    return ::PROTOBUF_NAMESPACE_ID::internal::ComputeUnknownFieldsSize(
        _internal_metadata_, total_size, &_cached_size_);
  }
  int cached_size = ::PROTOBUF_NAMESPACE_ID::internal::ToCachedSize(total_size);
  SetCachedSize(cached_size);
  return total_size;
}

void DetectedOccupant::MergeFrom(const ::PROTOBUF_NAMESPACE_ID::Message& from) {
// @@protoc_insertion_point(generalized_merge_from_start:osi3.DetectedOccupant)
  GOOGLE_DCHECK_NE(&from, this);
  const DetectedOccupant* source =
      ::PROTOBUF_NAMESPACE_ID::DynamicCastToGenerated<DetectedOccupant>(
          &from);
  if (source == nullptr) {
  // @@protoc_insertion_point(generalized_merge_from_cast_fail:osi3.DetectedOccupant)
    ::PROTOBUF_NAMESPACE_ID::internal::ReflectionOps::Merge(from, this);
  } else {
  // @@protoc_insertion_point(generalized_merge_from_cast_success:osi3.DetectedOccupant)
    MergeFrom(*source);
  }
}

void DetectedOccupant::MergeFrom(const DetectedOccupant& from) {
// @@protoc_insertion_point(class_specific_merge_from_start:osi3.DetectedOccupant)
  GOOGLE_DCHECK_NE(&from, this);
  _internal_metadata_.MergeFrom<::PROTOBUF_NAMESPACE_ID::UnknownFieldSet>(from._internal_metadata_);
  ::PROTOBUF_NAMESPACE_ID::uint32 cached_has_bits = 0;
  (void) cached_has_bits;

  candidate_.MergeFrom(from.candidate_);
  if (from.has_header()) {
    _internal_mutable_header()->::osi3::DetectedItemHeader::MergeFrom(from._internal_header());
  }
}

void DetectedOccupant::CopyFrom(const ::PROTOBUF_NAMESPACE_ID::Message& from) {
// @@protoc_insertion_point(generalized_copy_from_start:osi3.DetectedOccupant)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

void DetectedOccupant::CopyFrom(const DetectedOccupant& from) {
// @@protoc_insertion_point(class_specific_copy_from_start:osi3.DetectedOccupant)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

bool DetectedOccupant::IsInitialized() const {
  return true;
}

void DetectedOccupant::InternalSwap(DetectedOccupant* other) {
  using std::swap;
  _internal_metadata_.Swap<::PROTOBUF_NAMESPACE_ID::UnknownFieldSet>(&other->_internal_metadata_);
  candidate_.InternalSwap(&other->candidate_);
  swap(header_, other->header_);
}

::PROTOBUF_NAMESPACE_ID::Metadata DetectedOccupant::GetMetadata() const {
  return GetMetadataStatic();
}


// @@protoc_insertion_point(namespace_scope)
}  // namespace osi3
PROTOBUF_NAMESPACE_OPEN
template<> PROTOBUF_NOINLINE ::osi3::DetectedOccupant_CandidateOccupant* Arena::CreateMaybeMessage< ::osi3::DetectedOccupant_CandidateOccupant >(Arena* arena) {
  return Arena::CreateMessageInternal< ::osi3::DetectedOccupant_CandidateOccupant >(arena);
}
template<> PROTOBUF_NOINLINE ::osi3::DetectedOccupant* Arena::CreateMaybeMessage< ::osi3::DetectedOccupant >(Arena* arena) {
  return Arena::CreateMessageInternal< ::osi3::DetectedOccupant >(arena);
}
PROTOBUF_NAMESPACE_CLOSE

// @@protoc_insertion_point(global_scope)
#include <google/protobuf/port_undef.inc>
